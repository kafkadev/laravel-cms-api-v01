<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class UserMeta extends Model {
//https://stackoverflow.com/questions/16815551/how-to-do-this-in-laravel-subquery-where-in
//DB::table('user_metas')->whereNotIn('user_id', [10])->get()
//DB::table('users')->whereNotIn('user_id', [10])->get();
//https://laracasts.com/discuss/channels/general-discussion/update-multiple-records-using-eloquent
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','meta_name', 'meta_value',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
