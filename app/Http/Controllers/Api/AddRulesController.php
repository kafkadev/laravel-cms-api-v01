<?php namespace App\Http\Controllers\Api;

//use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddRulesController extends Controller
{
  public function __construct(){

  }

  public function addRules(Request $request){
    $formData = $request->all();
    /* veritabanı insert elemanları */
    $insertData = [];
    $insertData['camp_id'] = $formData['camp_id'];
    $insertData['user_id'] = $formData['user_id'];
    $insertData['kural_array'] = serialize($formData);
    $insertData['status'] = $formData['status'];
    DB::insert('insert into kurallar (camp_id, user_id, kural_array, status) values (?, ?, ?, ?)', array_values($insertData));
    return true;
  }
}
