<?php namespace App\Http\Controllers\Api;

//use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
  public function __construct(){

  }

  public function addCount(Request $request){


    $insertArray = array(
      'ad_id' => 1,
      'camp_id'   =>   1
    );
    $valuesArray = array(
      'click' => 1,
      'view' => 1
    );

    //app('db')->table('report')->insert($insertArray);
    app('db')->table('report')->updateOrInsert($insertArray, $valuesArray);
    $count = 0;
    return $count;
  }

  public function updateView($ad_id)
  {
    // app('db')->table('report')->where('ad_id', $ad_id)->increment('view');
    // app('redis')->set("counter", 0);
    // app('redis')->incr("counter");
    // app('cache')->put('saybak01', 1, 3600);
    // app('cache')->increment('saybak01', 1);
    // app('files')->append('file.log', "loglar01 \r\n");
    // app('files')->put(base_path('public/say01.json'), json_encode($sayidecode));
    // app('files')->get('say01.json');
    // app('files')->put(base_path('public/say01.json'), json_encode($sayim, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
  }

  public function updateClick($ad_id)
  {
    app('db')->table('report')->where('ad_id', $ad_id)->increment('click');
  }
}
