<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AuthenticateController extends Controller{

  public function postLogin(Request $req){

    $credentials = $req->only('email', 'password');
    /*
    http://adsdemo.dev/api/postlogin
    {
    "email" : "kafkadeveloper@gmail.com",
    "password" : "123456"
  }
  */

  /**
  * Token on success | false on fail
  *
  * @var string | boolean
  */
  $token = Auth::attempt($credentials);

  return ($token !== false)
  ? response()->json(['jwt' => $token], 200)
  : response('Unauthorized.', 401);

}

}
