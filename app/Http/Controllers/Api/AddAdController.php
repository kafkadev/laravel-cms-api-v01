<?php namespace App\Http\Controllers\Api;

//use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddAdController extends Controller
{
  public function __construct(){

  }

  public function addAds(Request $request){
    $currentForm = $request->all();
    $ad_body = $this->adBody($currentForm);


    /* veri tabanına kayıt edilecek array */
    $insertData = [];
    $insertData['ad_body'] = serialize($ad_body);
    $insertData['name'] = $currentForm['name'];
    $insertData['kampanya_id'] = $currentForm['kampanya_id'];
    $insertData['type'] = $currentForm['type'];
    $insertData['note'] = $currentForm['note'];
    $insertData['user_id'] = $currentForm['user_id'];
    app('db')->table('reklamlar')->insert($insertData);

    //app('db')->insert('insert into reklamlar (name, ad_body, kampanya_id, type, note, user_id) values (?, ?, ?, ?, ?, ?)', array_values($insertData));
    // echo "<pre>";
    return  response()->json($currentForm, 200);

  }

  public function updateAds(Request $request){
    $currentForm = $request->all();
    //$update_array = $request->all();
    $ad_body = $this->adBody($currentForm);


    /* veri tabanına kayıt edilecek array */
    $insertData = [];
    $insertData['ad_body'] = serialize($ad_body);
    $insertData['name'] = $currentForm['name'];
    $insertData['kampanya_id'] = $currentForm['kampanya_id'];
    $insertData['type'] = $currentForm['type'];
    $insertData['note'] = $currentForm['note'];
    $insertData['user_id'] = $currentForm['user_id'];


    app('db')->table('reklamlar')
    ->where('id', $currentForm['id'])
    ->where('user_id', $currentForm['user_id'])
    ->where('kampanya_id', $currentForm['kampanya_id'])
    ->update($insertData);

  }

  // {
  //   "id" : "29",
  //   "name": "3:52 reklam 46",
  //   "cikis_url": "http://www.alexa.com",
  //   "status": "1",
  //   "note": "fdgfgdfgdf",
  //   "user_id": "2",
  //   "kampanya_id": "9",
  //   "type": "banner",
  //   "current_image" : "MDAwMTIuanBn.jpg"
  // }
  public function adBody($array_value)
  {
    $ad_body = [];
    /* form içindeki banner yükleniyor /helpers.php */
    if (isset($array_value['imaj'])) {
      $fileName = bannerUpload($array_value['imaj']);
      $ad_body['imaj'] = $fileName;
    } elseif(isset($array_value['current_image'])){
      $ad_body['imaj'] = $array_value['current_image'];
    };
    /* veri tabanına ad_body olarak kayıt edilecek array elemanı */
    $ad_body['size'] = $array_value['type'];
    $ad_body['target_url'] = $array_value['cikis_url'];
    $ad_body['alt_tag'] = $array_value['name'];
    $ad_body['type'] = $array_value['type'];
    return $ad_body;
    // echo "<pre>";
    // print_r(bannerUpload($array_value['imaj']));
  }
}
