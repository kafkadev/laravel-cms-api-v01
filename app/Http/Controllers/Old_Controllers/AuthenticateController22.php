<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;

class AuthenticateController extends Controller{
	
public function postLogin(Request $req){

    $credentials = $req->only('email', 'password');

    /**
     * Token on success | false on fail
     *
     * @var string | boolean
     */
    $token = Auth::attempt($credentials);

    return ($token !== false)
            ? json_encode(['jwt' => $token])
            : response('Unauthorized.', 401);

}

}