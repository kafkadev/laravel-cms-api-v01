<?php
namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PersonalController extends Controller
{
  protected $redirectTo = '/home';

  public function __construct()
  {

  }

public function profileUpdate(Request $request, $username)
{
  $formlar = $request->input('formlar');
  $datam = [];
  foreach ($formlar as $key => $item) {
    $datam[] = UserMeta::updateOrCreate([
      'user_id'   => 1,
      'meta_name' => $key
  ],[
    'meta_name' => $key,
    'meta_value' => $item
  ]);
  }

return response()->json($datam, 200);
}

public function profileUser($username)
{
  $posts = DB::connection('wordpress')->table('posts')->where('post_type', 'articles')->where('post_status', 'publish')->limit(10)->get();
  $user_info = DB::table('user_metas')->where('user_id', $username)->where('meta_name', 'user_info')->first();
  $data['user_info'] = unserialize($user_info->meta_value);
  $data['posts'] =   $posts;
//  return response()->json($data, 200);
return view('profiles.personal.index', $data);
}

public function profileActivity($username)
{
  $posts = DB::connection('wordpress')->table('posts')->where('post_type', 'articles')->where('post_status', 'publish')->limit(10)->get();
  $user_info = DB::table('user_metas')->where('user_id', $username)->where('meta_name', 'user_info')->first();
  $data['user_info'] = unserialize($user_info->meta_value);
  $data['posts'] =   $posts;
//  return response()->json($data, 200);
return view('profiles.personal.index', $data);
}
public function profileFriends($username)
{

  $user_info = DB::table('user_metas')->where('user_id', $username)->where('meta_name', 'user_info')->first();
  $data['user_info'] = unserialize($user_info->meta_value);
//  return response()->json($data, 200);
return view('profiles.personal.friends', $data);
}

public function profileInfo($username)
{

  $user_info = DB::table('user_metas')->where('user_id', $username)->where('meta_name', 'user_info')->first();
  $data['user_info'] = unserialize($user_info->meta_value);
//  return response()->json($data, 200);
return view('profiles.personal.info', $data);
}
public function profilePortfolio($username)
{

  $user_info = DB::table('user_metas')->where('user_id', $username)->where('meta_name', 'user_info')->first();
  $data['user_info'] = unserialize($user_info->meta_value);
//  return response()->json($data, 200);
return view('profiles.personal.info', $data);
}


}
