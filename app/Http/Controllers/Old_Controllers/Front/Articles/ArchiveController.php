<?php
namespace App\Http\Controllers\Front\Articles;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ArchiveController extends Controller
{
    protected $redirectTo = '/home';

    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function index()
    {
      $data = [];
      $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/articles");
      $data['articles'] = json_decode($getArticle);
      return view('archives.article', $data);
    }
    public function courses()
    {
      $data = [];
      $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/courses");
      $data['courses'] = json_decode($getArticle);
      return view('archives.courses', $data);
    }
    public function classifieds()
    {
      $data = [];
      $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/classified");
      $data['classifieds'] = json_decode($getArticle);
      return view('archives.classifieds', $data);
    }
    public function classifiedsCategory()
    {
      $data = [];
      $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/classified");
      $data['classifieds'] = json_decode($getArticle);
      return view('archives.classifieds_category', $data);
    }
    public function adminContents()
    {

      $data = [];
      $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/classified");
      $data['classifieds'] = json_decode($getArticle);
      return view('admin.root', $data);
    }
    public function adminPages($deneme, $accountIdo)
    {


echo $deneme . '<br>';
echo $accountIdo . '<br>';

    }

    public function profileUser($username)
    {
      $user_info = DB::table('user_metas')->where('user_id', '1')->where('meta_name', 'user_info')->first();
      //var_dump($user_info->meta_value);
      $data = [];
      $getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/classified");
      $data['classifieds'] = json_decode($getArticle);
      $data['user_info'] = unserialize($user_info->meta_value);
      return view('profiles.personal.index', $data);
    }

  }
