<?php
namespace App\Providers;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;
class CustomServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('custom', function ($request) {

            if ($request->header('api-token')) {
                return User::where('api_token', $request->header('api-token'))->first();
            } elseif ($request->input('api_token')) {
                 return User::where('api_token', $request->input('api_token'))->first();
            } else {
                return User::where('api_token', $request->session()->get('user.api_token'))->first();
            }

        }); 

        /*       $this->app['auth']->viaRequest('custom', function ($request) {
            return $request->session()->get('_token') ? $request->session()->get('_token') : null;
        });*/
    }
}