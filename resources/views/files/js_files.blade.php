
<!-- Core JS files -->
<script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>
<script type="text/javascript" src="/assets/js/plugins/ui/nicescroll.min.js"></script>




@yield('custom_js')
<script type="text/javascript" src="/assets/js/core/app.js"></script>

<!-- Detailed task
<div id="includedContent"></div>

<object name="foo" type="text/html" data="/demohtml/ana.html"></object>
Alternatively:
<embed type="text/html" src="/demohtml/ana.html">
 -->
<script type="text/javascript">
// $(function(){
//   $("#includedContent").load("/demohtml/ana.html");
// });


</script>
