<!-- Secondary sidebar -->
<div class="sidebar sidebar-main sidebar-default sidebar-separate">

  <div class="sidebar-content">



    <!-- Main navigation -->
    <div class="sidebar-category sidebar-category-visible">

      <div class="category-title">
        <span>Navigation</span>
        <ul class="icons-list">
          <li><a href="#" data-action="collapse"></a></li>
        </ul>
      </div>

      <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
          <!-- Main -->

          <li><a href="/kampanyalar/{{$kampanyaId}}/raporlar"><i class="icon-statistics"></i> <span>Raporlar</span></a></li>
          <li><a href="/kampanyalar/{{$kampanyaId}}/reklamlar"><i class="icon-list-unordered"></i> <span>Reklamlar</span></a></li>
          <li><a href="/kampanyalar/{{$kampanyaId}}/lokasyon"><i class="icon-home4"></i> <span>Lokasyon</span></a></li>
          <li><a href="/kampanyalar/{{$kampanyaId}}/platform"><i class="icon-stack2"></i> <span>Platform</span></a></li>
          <li><a href="/kampanyalar/{{$kampanyaId}}/filtreler"><i class="icon-stack2"></i> <span>Filtreler</span></a></li>
          <li><a href="/kampanyalar/{{$kampanyaId}}/zamanlama"><i class="icon-home4"></i> <span>Zamanlama</span></a></li>
          <li><a href="/kampanyalar/{{$kampanyaId}}/yonetim"><i class="icon-cog3"></i> <span>Yönetim</span></a></li>

        </ul>
      </div>
    </div>
    <!-- /main navigation -->

  </div>
</div>
<!-- /secondary sidebar -->
