@extends('layouts.general_layout', array())
@section('custom_js')
<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="assets/js/pages/form_inputs.js"></script>
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
<script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>
<!-- /theme JS files -->
@endsection
@section('content')
@include('headers.dashboard')

<!-- Page container -->
<div class="page-container">

	<!-- Page content -->
	<div class="page-content">
		@include('widgets.kampanya_sidebar', array('title' => 'classified'))
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Detailed task -->
			<div class="row">


				<div class="col-lg-12" style="">


					<div class="panel panel-flat">


						<div class="panel-body">
							<form class="form-horizontal" action="/api/reklam-ekle" method="post" enctype="multipart/form-data">


							        <fieldset class="content-group">
							          <div class="form-group">
							            <label class="control-label col-lg-2">Reklam Adı</label>
							            <div class="col-lg-10">
							              <div class="form-control-static">Yatay Reklam</div>
							            </div>
							          </div>


							          <div class="form-group">
							            <label class="col-lg-2 control-label">Single file upload:</label>
							            <div class="col-lg-10">
							              <input type="file" name="imaj" class="file-input">
							              <span class="help-block">Automatically convert a file input to a bootstrap file input widget by setting its class as <code>file-input</code>.</span>
							            </div>
							          </div>





							          <div class="form-group">
							            <label class="control-label col-lg-2">Reklam Adı</label>
							            <div class="col-lg-10">
							              <input name="name" type="text" class="form-control" placeholder="Enter a capaign name">
							            </div>
							          </div>




							          <div class="form-group">
							            <label class="control-label col-lg-2">Çıkış Adresi</label>
							            <div class="col-lg-10">
							              <input name="cikis_url" type="text" class="form-control" value="http://">
							            </div>
							          </div>

							          <div class="form-group">
							            <label class="control-label col-lg-2">Yayın Durumu</label>
							            <div class="col-lg-10">
							              <select name="status" class="form-control">
							                <option value="1">Aktif</option>
							                <option value="0">Bekliyor</option>
							              </select>
							            </div>
							          </div>





							          <div class="form-group">
							            <label class="control-label col-lg-2">Notlar</label>
							            <div class="col-lg-10">
							              <textarea rows="5" cols="5" name="note" class="form-control" placeholder="Default textarea"></textarea>
							            </div>
							          </div>
							        </fieldset>



    <input name="user_id" type="hidden" value="{!! getUserInfo()['user_id'] !!}">
    <input name="kampanya_id" type="hidden" value="{{$kampanyaId}}">
    <input name="type" type="hidden" value="banner">
							      <div class="text-right">
							        <button type="submit" class="btn btn-primary">Gönder <i class="icon-arrow-right14 position-right"></i></button>
							      </div>






							    </div>
							  </div>
							</form>

						</div>
					</div>



				</div>

			</div>
			<!-- /detailed task -->
		</div>
		<!-- /main content -->
	</div>
	<!-- /page content -->
</div>
<!-- /page container -->
@endsection
