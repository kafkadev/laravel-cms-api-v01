@extends('layouts.general_layout', array())
@section('custom_js')
@endsection
@section('content')
@include('headers.general_header')

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">
          @include('widgets.general_sidebar', array('title' => 'classified'))
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">


        <div class="col-lg-12" style="">
        <!-- Marketing campaigns -->
        <div class="panel panel-flat">
          <div class="panel-heading">
            <h5 class="panel-title">Marketing campaigns</h5>
            <div class="heading-elements">
              <span class="label bg-success heading-text">28 active</span>
              <ul class="icons-list">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-sync"></i> Update data</a></li>
                    <li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>
                    <li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>
                  </ul>
                        </li>
                      </ul>
                    </div>
          </div>

          <div class="table-responsive">
            <table class="table table-lg text-nowrap">
              <tbody>
                <tr>
                  <td class="col-md-5">
                    <div class="media-left">
                      <div id="campaigns-donut"></div>
                    </div>

                    <div class="media-left">
                      <h5 class="text-semibold no-margin">38,289 <small class="text-success text-size-base"><i class="icon-arrow-up12"></i> (+16.2%)</small></h5>
                      <ul class="list-inline list-inline-condensed no-margin">
                        <li>
                          <span class="status-mark border-success"></span>
                        </li>
                        <li>
                          <span class="text-muted">May 12, 12:30 am</span>
                        </li>
                      </ul>
                    </div>
                  </td>

                  <td class="col-md-5">
                    <div class="media-left">
                      <div id="campaign-status-pie"></div>
                    </div>

                    <div class="media-left">
                      <h5 class="text-semibold no-margin">2,458 <small class="text-danger text-size-base"><i class="icon-arrow-down12"></i> (- 4.9%)</small></h5>
                      <ul class="list-inline list-inline-condensed no-margin">
                        <li>
                          <span class="status-mark border-danger"></span>
                        </li>
                        <li>
                          <span class="text-muted">Jun 4, 4:00 am</span>
                        </li>
                      </ul>
                    </div>
                  </td>

                  <td class="text-right col-md-2">
                    <a href="#" class="btn bg-indigo-300"><i class="icon-statistics position-left"></i> View report</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="table-responsive">
            <table class="table text-nowrap">
              <thead>
                <tr>
                  <th>Campaign</th>
                  <th class="col-md-2">Client</th>
                  <th class="col-md-2">Budget</th>
                  <th class="col-md-2">Status</th>
                </tr>
              </thead>
              <tbody>
                <tr class="active border-double">
                  <td colspan="3">Today</td>
                  <td class="text-right">
                    <span class="progress-meter" id="today-progress" data-progress="30"></span>
                  </td>
                </tr>

  @for($i = 0; $i < 12; $i ++)


                <tr>
                  <td>
                    <div class="media-left media-middle">
                      <a href="#"><img src="http://lumensession.dev/assets/images/placeholder.jpg" class="img-circle img-lg" alt=""></a>
                    </div>
                    <div class="media-left">
                      <div class=""><a href="" class="text-default text-semibold">title {!! $i !!}</a></div>
                      <div class="text-muted text-size-small">
                        <span class="status-mark border-blue position-left"></span>
                        02:00 - 03:00
                      </div>
                    </div>
                  </td>
                  <td><span class="text-muted">Mintlime</span></td>
                  <td><h6 class="text-semibold">$5,489</h6></td>
                  <td><span class="label bg-blue">Active</span></td>
                </tr>

@endfor



                <tr class="active border-double">
                  <td colspan="3">Yesterday</td>
                  <td class="text-right">
                    <span class="progress-meter" id="yesterday-progress" data-progress="65"></span>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="media-left media-middle">
                      <a href="#"><img src="assets/images/brands/bing.png" class="img-circle img-xs" alt=""></a>
                    </div>
                    <div class="media-left">
                      <div class=""><a href="#" class="text-default text-semibold">Bing campaign</a></div>
                      <div class="text-muted text-size-small">
                        <span class="status-mark border-success position-left"></span>
                        15:00 - 16:00
                      </div>
                    </div>
                  </td>
                  <td><span class="text-muted">Metrics</span></td>
                  <td><h6 class="text-semibold">$970</h6></td>
                  <td><span class="label bg-success-400">Pending</span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /marketing campaigns -->
      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
