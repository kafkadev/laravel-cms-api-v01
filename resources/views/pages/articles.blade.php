@extends('layouts.general_layout', array())
@section('custom_js')
@endsection
@section('content')
@include('headers.front_header')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
@include('widgets.general_sidebar', array('title' => ''))
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">
        <div class="col-lg-12">
@include('parts.for_articles', array('title' => ''))
      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@include('footers.general_footer')
@endsection
