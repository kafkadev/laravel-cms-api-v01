@extends('layouts.general_layout', array())
@section('custom_js')
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
@endsection
@section('content')
@include('headers.business_header')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">
        <div class="col-lg-8">
          <!-- Submit a ticket -->
          <div class="panel panel-body stack-media-on-mobile">
            <div class="media-left">
              <a href="#" class="btn btn-link btn-icon text-teal">
                <i class="icon-question7 icon-2x no-edge-top"></i>
              </a>
            </div>
            <div class="media-body media-middle">
              <h6 class="media-heading text-semibold">Can't find what you're looking for?</h6>
              Maladroit forgetfully under until the fraternally on one much whispered waked much cumulatively some rabidly after thanks hey
            </div>
            <div class="media-right media-middle">
              <a href="#" class="btn bg-warning-400 btn-lg"><i class="icon-mail5 position-left"></i> Submit a ticket</a>
            </div>
          </div>
          <!-- /submit a ticket -->
          <div class="panel panel-white">
            <div class="panel-heading">
              <h6 class="panel-title">Hakkımızda</h6>
              <div class="heading-elements">
                <ul class="icons-list">
                  <li><a data-action="collapse"></a></li>
                  <li><a data-action="reload"></a></li>
                  <li><a data-action="close"></a></li>
                </ul>
              </div>
            </div>
            <div class="panel-body">
              White panel using <code>.panel-white</code> class
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="panel panel-body">
                <div class="media">
                  <div class="media-left">
                    <a href="#"><i class="icon-file-text2 text-success-400 icon-2x no-edge-top mt-5"></i></a>
                  </div>
                  <div class="media-body">
                    <h6 class="media-heading text-semibold"><a href="#" class="text-default">Walking away fallaciously</a></h6>
                    Ouch licentiously lackadaisical crud together began gregarious below near darn goodness
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="panel panel-body">
                <div class="media">
                  <div class="media-left">
                    <a href="#"><i class="icon-file-play text-warning-400 icon-2x no-edge-top mt-5"></i></a>
                  </div>
                  <div class="media-body">
                    <h6 class="media-heading text-semibold"><a href="#" class="text-default">Nutria and rewound</a></h6>
                    Strove the darn hey as far oh alas and yikes and gosh knitted this slept via gerbil baneful
                  </div>
                </div>
              </div>
            </div>
          </div>
          <ul class="list-group bg-white mb-20">
            <li class="list-group-item">
              <h6 class="list-group-item-heading"><i class="icon-car position-left"></i> Leapt so heedlessly</h6>
              <p class="list-group-item-text">Haltered disconsolate cocky grizzly rode said oh outgrew patiently wild empirically near this and a alas some more</p>
            </li>
            <li class="list-group-item">
              <h6 class="list-group-item-heading"><i class="icon-bus position-left"></i> Black where yikes</h6>
              <p class="list-group-item-text">This and shivered wow boa yikes additional much one lavish gasped outside amongst jeez scurrilously and octopus</p>
            </li>
            <li class="list-group-item">
              <h6 class="list-group-item-heading"><i class="icon-train2 position-left"></i> Gecko preparatory</h6>
              <p class="list-group-item-text">Insincere dipped flauntingly yikes therefore or more clenched but beneath krill before dear however</p>
            </li>
            <li class="list-group-divider"></li>
            <li class="list-group-item">
              <h6 class="list-group-item-heading"><i class="icon-people position-left"></i> Parrot slid wow</h6>
              <p class="list-group-item-text">Gosh plankton thus egotistically alas satisfactorily flatly towards and far therefore oh drove convenient less</p>
            </li>
          </ul>
          @include('parts.for_articles', array('title' => ''))
        </div>
        <div class="col-md-4">
        <style media="screen">
        .col-md-3 .thumbnail{
          /*margin-top: -220px;*/
        }
        </style>
        @include('parts.right_business', array('title' => ''))
        <div class="panel panel-body">
          <div class="media">
            <div class="media-left">
              <a href="#"><i class="icon-file-play text-warning-400 icon-2x no-edge-top mt-5"></i></a>
            </div>
            <div class="media-body">
              <h6 class="media-heading text-semibold"><a href="#" class="text-default">Nutria and rewound</a></h6>
              Strove the darn hey as far oh alas and yikes and gosh knitted this slept via gerbil baneful
            </div>
          </div>
        </div>
      </div>
      </div>
  </div>
  <!-- /detailed task -->
</div>
<!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
