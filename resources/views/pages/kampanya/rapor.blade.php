@extends('layouts.general_layout', array())
@section('custom_js')

<!-- /theme JS files -->
@endsection
@section('content')
@include('headers.dashboard')

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">
          @include('widgets.kampanya_sidebar', array('title' => 'classified'))
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">


        <div class="col-lg-12" style="">

					<!-- Invoice grid options -->
					<div class="navbar navbar-default navbar-xs navbar-component">
						<ul class="nav navbar-nav no-border visible-xs-block">
							<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
						</ul>

						<div class="navbar-collapse collapse" id="navbar-filter">
							<p class="navbar-text">Filter:</p>
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-time-asc position-left"></i> By date <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="#">Show all</a></li>
										<li class="divider"></li>
										<li><a href="#">Today</a></li>
										<li><a href="#">Yesterday</a></li>
										<li><a href="#">This week</a></li>
										<li><a href="#">This month</a></li>
										<li><a href="#">This year</a></li>
									</ul>
								</li>

								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-amount-desc position-left"></i> By status <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="#">Show all</a></li>
										<li class="divider"></li>
										<li><a href="#">Open</a></li>
										<li><a href="#">On hold</a></li>
										<li><a href="#">Resolved</a></li>
										<li><a href="#">Closed</a></li>
										<li><a href="#">Dublicate</a></li>
										<li><a href="#">Invalid</a></li>
										<li><a href="#">Wontfix</a></li>
									</ul>
								</li>

								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-sort-numeric-asc position-left"></i> By priority <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="#">Show all</a></li>
										<li class="divider"></li>
										<li><a href="#">Highest</a></li>
										<li><a href="#">High</a></li>
										<li><a href="#">Normal</a></li>
										<li><a href="#">Low</a></li>
									</ul>
								</li>
							</ul>

							<div class="navbar-right">
								<p class="navbar-text">Sorting:</p>
								<ul class="nav navbar-nav">
									<li class="active"><a href="#"><i class="icon-sort-alpha-asc position-left"></i> Asc</a></li>
									<li><a href="#"><i class="icon-sort-alpha-desc position-left"></i> Desc</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /invoice grid options -->


					<!-- Invoice grid -->
					<div class="text-center content-group text-muted content-divider">
						<span class="pt-10 pb-10">Today</span>
					</div>

					<!-- Small table -->
					<div class="panel panel-flat">


						<div class="table-responsive">
							<table class="table table-sm">
								<thead>
									<tr>
										<th>#date</th>
										<th>Tıklama</th>
										<th>Gösterim</th>
										<th>CTR</th>
									</tr>
								</thead>
								<tbody>
									@for($i = 0; $i < 6; $i ++)
									<tr>
										<td>Gün {{$i}}</td>
										<td><?php echo rand(100, 500); ?></td>
										<td><?php echo rand(1000, 15000); ?></td>
										<td><?php echo rand(1, 3); ?>%</td>
									</tr>
									@endfor
								</tbody>
							</table>
						</div>
					</div>
					<!-- /small table -->



      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
