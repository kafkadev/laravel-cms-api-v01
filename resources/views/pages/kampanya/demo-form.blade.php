@extends('layouts.general_layout', array())
@section('custom_js')
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
  <script type="text/javascript" src="assets/js/pages/form_inputs.js"></script>
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
<script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>
<!-- /theme JS files -->
@endsection
@section('content')
@include('headers.dashboard')

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">
          @include('widgets.kampanya_sidebar', array('title' => 'classified'))
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">


        <div class="col-lg-12" style="">

          <!-- Form horizontal -->
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h5 class="panel-title">Lokasyon</h5>
  						<div class="heading-elements">
  							<ul class="icons-list">
  		                		<li><a data-action="collapse"></a></li>
  		                		<li><a data-action="reload"></a></li>
  		                		<li><a data-action="close"></a></li>
  		                	</ul>
  	                	</div>
  					</div>

  					<div class="panel-body">
<form class="form-horizontal" action="/api/kampanyaekle" method="post" enctype="multipart/form-data">

	<div class="form-group">
		<label class="control-label col-lg-2">Lokasyonlar</label>
		<div class="col-lg-10">
										<select name="lokasyonlar[]" multiple="multiple" class="form-control">
												<option value="tr" selected="selected">Türkiye</option>
												<option  value="ru" selected="selected">Russia</option>
												<option  value="fr" >France</option>
												<option  selected="selected">Germany</option>

										</select>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-lg-2">Diller</label>
		<div class="col-lg-10">
										<select name="diller[]" multiple="multiple" class="form-control">
												<option selected="selected">Türkçe</option>
												<option selected="selected">English</option>
												<option>French</option>
												<option>Russian</option>
										</select>
		</div>
	</div>


  <div class="form-group">
    <label class="control-label col-lg-2">Browser</label>
    <div class="col-lg-10">
                    <select name="browsers[]" multiple="multiple" class="form-control">
                        <option selected="selected">Chrome</option>
                        <option selected="selected">Firefox</option>
                        <option>Yandex</option>
                        <option selected="selected">Internet Explorer</option>

                    </select>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-lg-2">Network</label>
    <div class="col-lg-10">
                    <select name="notwork[]" multiple="multiple" class="form-control">
                        <option selected="selected">Mobil</option>
                        <option selected="selected">Wifi</option>
                        <option>Lan</option>
                        <option selected="selected">Intranet</option>

                    </select>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-2">Max Tıklama</label>
    <div class="col-md-10">
      <input class="form-control" type="number" name="tiklama">
      <span class="help-block">Max Tıklanma Sayısı <code>default=~~</code></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-2">Max Gösterim</label>
    <div class="col-md-10">
      <input class="form-control" type="number" name="gosterim">
      <span class="help-block">Max Gösterim Sayısı <code>default=~~</code></span>
    </div>
  </div>




  <div class="form-group">
    <label class="control-label col-lg-2">Sayım Türü</label>
    <div class="col-lg-10">
      <select name="sayim_turu" class="form-control">
        <option value="1">Banner</option>
        <option value="0">Feed</option>
      </select>
    </div>
  </div>


  <div class="form-group">
    <label class="control-label col-md-2">Başlangıç Tarihi</label>
    <div class="col-md-10">
      <input name="start_date" type="date" class="form-control" value="01/01/2015">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-2">Bitiş Tarihi</label>
    <div class="col-md-10">
      <input name="end_date" type="date" class="form-control" value="01/01/2015">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-2">Başlangıç Saati</label>
    <div class="col-md-10">
      <input name="start_time" type="time" class="form-control" value="00:00">
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-2">Bitiş Saati</label>
    <div class="col-md-10">
      <input name="end_time" type="time" class="form-control" value="00:00">
    </div>
  </div>
      <input name="camp_id" type="hidden" class="form-control" value="{{$kampanyaId}}">
      <input name="user_id" type="hidden" class="form-control" value="1">
      <input name="status" type="hidden" class="form-control" value="1">
  							<div class="text-right">
  								<button type="submit" class="btn btn-primary">Güncelle <i class="icon-arrow-right14 position-right"></i></button>
  							</div>
  						</form>
  					</div>
  				</div>
  				<!-- /form horizontal -->






      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
