@extends('layouts.general_layout', array())
@section('custom_js')
@endsection
@section('content')
@include('headers.front_header')
<!-- Page container -->
<div class="page-container">
  <!-- Page content -->
  <div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row" style="margin-top: -100px;">

        <div class="col-lg-9">
          @include('parts.single_panel', array('title' => ''))
        </div>
        <div class="col-md-3">
          @include('parts.right_profile', array('title' => ''))
        </div>

      </div>
      <!-- /detailed task -->
    </div>
    <!-- /main content -->
  </div>
  <!-- /page content -->
</div>
<!-- /page container -->
@include('footers.general_footer')
@endsection
