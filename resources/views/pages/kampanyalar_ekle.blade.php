@extends('layouts.general_layout', array())
@section('custom_js')
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
  <script type="text/javascript" src="assets/js/pages/form_inputs.js"></script>
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
<script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
<script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>
<!-- /theme JS files -->
@endsection
@section('content')
@include('headers.general_header')

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">
          @include('widgets.general_sidebar', array('title' => 'classified'))
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">


        <div class="col-lg-12" style="">


          <!-- Form horizontal -->
  				<div class="panel panel-flat">
  					<div class="panel-heading">
  						<h5 class="panel-title">Basic form inputs</h5>
  						<div class="heading-elements">
  							<ul class="icons-list">
  		                		<li><a data-action="collapse"></a></li>
  		                		<li><a data-action="reload"></a></li>
  		                		<li><a data-action="close"></a></li>
  		                	</ul>
  	                	</div>
  					</div>

  					<div class="panel-body">

  						<form class="form-horizontal" action="#">
  							<fieldset class="content-group">


  								<div class="form-group">
  									<label class="control-label col-lg-2">Kampanya Adı</label>
  									<div class="col-lg-10">
  										<input type="text" class="form-control">
  									</div>
  								</div>



  								<div class="form-group">
  									<label class="control-label col-lg-2">Kampanya Adı</label>
  									<div class="col-lg-10">
  										<input type="text" class="form-control" placeholder="Enter a capaign name">
  									</div>
  								</div>




  								<div class="form-group">
  									<label class="control-label col-lg-2">Kampanya Sitesi</label>
  									<div class="col-lg-10">
  										<input type="text" class="form-control" value="http://">
  									</div>
  								</div>

  		                        <div class="form-group">
  		                        	<label class="control-label col-lg-2">Kampanya Kategorisi</label>
  		                        	<div class="col-lg-10">
  			                            <select name="select" class="form-control">
  			                                <option value="opt1">Usual select box</option>
  			                                <option value="opt2">Option 2</option>
  			                                <option value="opt3">Option 3</option>
  			                                <option value="opt4">Option 4</option>
  			                                <option value="opt5">Option 5</option>
  			                                <option value="opt6">Option 6</option>
  			                                <option value="opt7">Option 7</option>
  			                                <option value="opt8">Option 8</option>
  			                            </select>
  		                            </div>
  		                        </div>



  								<div class="form-group">
  									<label class="control-label col-lg-2">Static text</label>
  									<div class="col-lg-10">
  										<div class="form-control-static">This is a static text</div>
  									</div>
  								</div>

  								<div class="form-group">
  									<label class="control-label col-lg-2">Notlar</label>
  									<div class="col-lg-10">
  										<textarea rows="5" cols="5" class="form-control" placeholder="Default textarea"></textarea>
  									</div>
  								</div>
  							</fieldset>


  							<div class="text-right">
  								<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
  							</div>
  						</form>
  					</div>
  				</div>
  				<!-- /form horizontal -->






      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
