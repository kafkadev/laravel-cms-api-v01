@extends('layouts.general_layout', array())
@section('custom_js')
<script type="text/javascript" src="assets/js/pages/support_chat_layouts.js"></script>
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
@endsection
@section('content')
@include('headers.general_header')

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">
          @include('widgets.sidebar_messages', array('title' => 'classified'))
    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">
        <div class="col-lg-12">
          <!-- Line content divider -->
    				<div class="panel panel-flat">
    					<div class="panel-heading">
    						<h6 class="panel-title">Line content divider</h6>
    						<div class="heading-elements">
    							<ul class="icons-list">
    		                		<li><a data-action="collapse"></a></li>
    		                		<li><a data-action="reload"></a></li>
    		                		<li><a data-action="close"></a></li>
    		                	</ul>
    	                	</div>
    					</div>

    					<div class="panel-body">
    						<ul class="media-list chat-stacked content-group">
                  @for($i = 0; $i < 8; $i ++)


                      							<li class="media">
                      								<div class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle" alt=""></div>
                      								<div class="media-body">
                      									<div class="media-heading">
                      										<a href="#" class="text-semibold">Margo Baker</a>
                      										<span class="media-annotation pull-right">2:03 pm <a href="#"><i class="icon-pin-alt position-right text-muted"></i></a></span>
                      									</div>
                      									Heard where and affecting dear hyena excluding hey confused the one
                      								</div>
                      							</li>


                @endfor

    						</ul>

    					</div>
    				</div>
    				<!-- /line content divider -->




      </div>

    </div>
    <!-- /detailed task -->
  </div>
  <!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
