@extends('layouts.general_layout', array())
@section('custom_js')
<script type="text/javascript" src="assets/js/plugins/ui/ripple.min.js"></script>
@endsection
@section('content')
@include('headers.business_header')

<!-- Page container -->
<div class="page-container">

  <!-- Page content -->
  <div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">
      <!-- Detailed task -->
      <div class="row">
        <div class="col-lg-9">


          @include('parts.for_articles', array('title' => ''))
        </div>
        <div class="col-md-3">

        @include('parts.right_menu', array('title' => ''))
        <div class="panel">
          <div class="panel-body text-center">
            <div class="icon-object border-warning-400 text-warning"><i class="icon-lifebuoy"></i></div>
            <h5 class="text-semibold">Support center</h5>
            <p class="content-group">Dear spryly growled much far jeepers vigilantly less and far hideous and some mannishly less jeepers less and and crud</p>
            <p><a href="#" class="btn bg-warning-400">Open a ticket</a></p>
          </div>
        </div>
      </div>


    </div>

  </div>
  <!-- /detailed task -->
</div>
<!-- /main content -->
</div>
<!-- /page content -->
</div>
<!-- /page container -->
@endsection
