<body class="navbar-bottom navbar-top layout-boxed">
  <!-- Page header -->
  <div class="page-header page-header-inverse bg-indigo">

    @include('parts.header_menu')


    <!-- /main navbar -->
    <style media="screen">
    .profile-cover{
background-color: black;
    }
    .profile-cover .media {
      position: absolute;
      bottom: 20px;
      left: 95px;
      right: 95px;
    }
    .profile-cover .media-right.media-middle > ul > li a.btn{
          text-transform: none;
    }
  .page-header.page-header-inverse{
      margin-bottom: 0;
    }
    </style>
    <!-- Page header content -->
    <div class="page-header-content" style="    padding: 0;
    margin: 0;
    width: 100%;">
      <!-- Cover area -->
      <div class="profile-cover">
        <div class="profile-cover-img" style="background-image: url(/assets/images/dogalgaz.jpg);height:220px;opacity:0.6;"></div>
        <div class="media">
          <div class="media-left">
            <a href="#" class="profile-thumb">
              <img src="/assets/images/dogalgaz-logo.jpg" class="img-circle-z" alt="">
            </a>
          </div>
          <div class="media-body">
            <h1>Doğa Ticaret <small class="display-block">Doğalgaz Sistemleri</small></h1>
          </div>
          <div class="media-right media-middle">
            <ul class="list-inline list-inline-condensed no-margin-bottom text-nowrap">
              <li><a href="#" class="btn btn-default"><i class="icon-file-picture position-left"></i> Cover image</a></li>
              <li><a href="#" class="btn btn-default"><i class="icon-file-stats position-left"></i> Statistics</a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /cover area -->
      <!-- <div class="page-title">
      <h4>Starters</h4>
    </div>
    <div class="heading-elements">
    <ul class="breadcrumb heading-text">
    <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
    <li><a href="layout_navbar_fixed_both.html">Starters</a></li>
    <li class="active">Fixed both</li>
  </ul>
</div> -->
</div>
<!-- /page header content -->

</div>
<!-- /page header -->

    @include('parts.navbar_business')
