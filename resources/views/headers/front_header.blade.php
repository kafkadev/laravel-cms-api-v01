<body class="navbar-bottom navbar-top layout-boxed">
  <!-- Page header -->
  <div class="page-header page-header-inverse bg-indigo">

    @include('parts.header_menu')


    <!-- Page header content -->
    <div class="page-header-content">
      <div class="page-title">
        <h4>Page Title</h4>
      </div>

    </div>
    <!-- /page header content -->
    @include('parts.navbar_manage')
  </div>
  <!-- /page header -->
