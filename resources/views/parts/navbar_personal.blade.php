<!-- Second navbar -->
<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top" id="navbar-second">
      <div class="navbar-boxed">


                <ul class="nav navbar-nav visible-xs-block">
                  <li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter" class="legitRipple"><i class="icon-menu7"></i></a></li>
                </ul>
                <div class="navbar-collapse collapse" id="navbar-filter">
                  <ul class="nav navbar-nav">
                    <li><a href="/profile/1/info" class="legitRipple"><i class="icon-user position-left"></i> Profil</a></li>
                    <li><a href="/profile/1/activity" class="legitRipple"><i class="icon-calendar3 position-left"></i> Aktivite</a></li>
                    <li><a href="/profile/1/following" class="legitRipple"><i class="icon-collaboration position-left"></i> Takip Ediliyor <span class="badge badge-success badge-inline position-right">32</span></a></li>
                      <li><a href="#" class="legitRipple"><i class="icon-images3 position-left"></i> Portfolyo</a></li>
                  </ul>
                  <div class="navbar-right">
                    <ul class="nav navbar-nav">

                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle legitRipple" data-toggle="dropdown"><i class="icon-gear"></i> <span class="visible-xs-inline-block position-right"> Options</span> <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                          <li><a href="#"><i class="icon-image2"></i> Update cover</a></li>
                          <li><a href="#"><i class="icon-clippy"></i> Update info</a></li>
                          <li><a href="#"><i class="icon-make-group"></i> Manage sections</a></li>
                          <li class="divider"></li>
                          <li><a href="#"><i class="icon-three-bars"></i> Activity log</a></li>
                          <li><a href="#"><i class="icon-cog5"></i> Profile settings</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>

</div>
</div>
<!-- /second navbar -->
