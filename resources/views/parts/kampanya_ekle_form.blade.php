<form class="form-horizontal" action="/api/reklamekle" method="post">
  <fieldset class="content-group">
    <div class="form-group">
      <label class="control-label col-lg-2">Kampanya Adı</label>
      <div class="col-lg-10">
        <input name="name" type="text" class="form-control" placeholder="örn: ayakkabı sezonu">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-lg-2">Kampanya Türü</label>
      <div class="col-lg-10">
        <select name="type" class="form-control">
          <option value="banner">Banner</option>
          <option value="feed">Feed</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-lg-2">Notlar</label>
      <div class="col-lg-10">
        <textarea name="notes" rows="5" cols="5" class="form-control" placeholder="Default textarea"></textarea>
      </div>
    </div>
  </fieldset>
    <input name="user_id" type="hidden" value='1'>
  <div class="text-right">
    <button type="submit" class="btn btn-primary">Gönder <i class="icon-arrow-right14 position-right"></i></button>
  </div>
</form>
