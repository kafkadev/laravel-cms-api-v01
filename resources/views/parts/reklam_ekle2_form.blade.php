<form class="form-horizontal" action="/api/reklamekle" method="post">
  <div class="tabbable">

    <ul class="nav nav-tabs nav-tabs-bottom">
      <li class="active"><a href="#right-icon-tab1" data-toggle="tab">Tanımlama <i class="icon-menu7 position-right"></i></a></li>
      <li><a href="#right-icon-tab2" data-toggle="tab">Filtreler <i class="icon-mention position-right"></i></a></li>
      <li><a href="#right-icon-tab3" data-toggle="tab">Ayarlar <i class="icon-mention position-right"></i></a></li>
      <li><a href="#right-icon-tab4" data-toggle="tab">Özellikler <i class="icon-mention position-right"></i></a></li>

    </ul>

    <div class="tab-content">

      <div class="tab-pane active" id="right-icon-tab1">

        <fieldset class="content-group">
          <div class="form-group">
            <label class="control-label col-lg-2">Reklam Adı</label>
            <div class="col-lg-10">
              <div class="form-control-static">Yatay Reklam</div>
            </div>
          </div>


          <div class="form-group">
            <label class="col-lg-2 control-label">Single file upload:</label>
            <div class="col-lg-10">
              <input type="file" name="imaj" class="file-input">
              <span class="help-block">Automatically convert a file input to a bootstrap file input widget by setting its class as <code>file-input</code>.</span>
            </div>
          </div>





          <div class="form-group">
            <label class="control-label col-lg-2">Reklam Adı</label>
            <div class="col-lg-10">
              <input name="reklam_adi" type="text" class="form-control" placeholder="Enter a capaign name">
            </div>
          </div>




          <div class="form-group">
            <label class="control-label col-lg-2">Çıkış Adresi</label>
            <div class="col-lg-10">
              <input name="cikis_url" type="text" class="form-control" value="http://">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-lg-2">Yayın Durumu</label>
            <div class="col-lg-10">
              <select name="status" class="form-control">
                <option value="1">Aktif</option>
                <option value="0">Bekliyor</option>
              </select>
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-lg-2">Reklam Ölçüsü</label>
            <div class="col-lg-10">
              <select name="status" class="form-control">
                <option value="1">300x250</option>
                <option value="0">468x60</option>
              </select>
            </div>
          </div>





          <div class="form-group">
            <label class="control-label col-lg-2">Notlar</label>
            <div class="col-lg-10">
              <textarea rows="5" cols="5" class="form-control" placeholder="Default textarea"></textarea>
            </div>
          </div>
        </fieldset>
      </div>


      <div class="tab-pane" id="right-icon-tab2">

        <fieldset class="content-group">

          <div class="form-group">
            <label class="col-lg-2 control-label">Single file upload:</label>
            <div class="col-lg-10">
              <input name="baska_imaj" type="file" class="file-input">
              <span class="help-block">Automatically convert a file input to a bootstrap file input widget by setting its class as <code>file-input</code>.</span>
            </div>
          </div>



        </fieldset>


      </div>

      <div class="tab-pane" id="right-icon-tab3">
        <fieldset class="content-group">

          <div class="form-group">
            <label class="control-label col-lg-2">Reklam Adı</label>
            <div class="col-lg-10">
              <input name="baska_name" type="text" class="form-control" placeholder="Enter a capaign name">
            </div>
          </div>



        </fieldset>
      </div>

      <div class="text-right">
        <button type="submit" class="btn btn-primary">Gönder <i class="icon-arrow-right14 position-right"></i></button>
      </div>






    </div>
  </div>
</form>
