<!-- Navigation -->
<div class="panel panel-flat">


	<div class="panel-body">
		<a href="#" class="btn bg-teal btn-block">Ask question <i class="icon-comment position-right"></i></a>
	</div>

	<div class="list-group no-border">
		<a href="#" class="list-group-item"><i class="icon-lifebuoy"></i> Help center</a>
		<a href="#" class="list-group-item"><i class="icon-book"></i> Knowledgebase</a>
		<a href="#" class="list-group-item"><i class="icon-reading"></i> Articles <span class="badge badge-danger pull-right">390</span></a>
		<a href="#" class="list-group-item"><i class="icon-graduation"></i> Tutorials</a>
		<a href="#" class="list-group-item"><i class="icon-book-play"></i> Video tutorials <span class="badge badge-primary pull-right">78</span></a>
		<div class="list-group-divider"></div>
		<a href="#" class="list-group-item"><i class="icon-comment"></i> Ask our community</a>
		<a href="#" class="list-group-item"><i class="icon-mail5"></i> Contact us</a>
	</div>
</div>
<!-- /navigation -->
