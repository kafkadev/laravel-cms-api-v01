
<!-- Task details -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h6 class="panel-title"><i class="icon-files-empty position-left"></i> Task details</h6>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
				<li><a data-action="reload"></a></li>
				<li><a data-action="close"></a></li>
			</ul>
		</div>
	</div>
	<table class="table table-borderless table-xs content-group-sm">
		<tbody>
			<tr>
				<td><i class="icon-briefcase position-left"></i> Project:</td>
				<td class="text-right"><span class="pull-right"><a href="#">Singular app</a></span></td>
			</tr>
			<tr>
				<td><i class="icon-alarm-add position-left"></i> Updated:</td>
				<td class="text-right">12 May, 2015</td>
			</tr>
			<tr>
				<td><i class="icon-alarm-check position-left"></i> Created:</td>
				<td class="text-right">25 Feb, 2015</td>
			</tr>
			<tr>
				<td><i class="icon-circles2 position-left"></i> Priority:</td>
				<td class="text-right">
					<div class="btn-group">
						<a href="#" class="label label-danger dropdown-toggle" data-toggle="dropdown">Highest <span class="caret"></span></a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li><a href="#"><span class="status-mark position-left bg-danger"></span> Highest priority</a></li>
							<li><a href="#"><span class="status-mark position-left bg-info"></span> High priority</a></li>
							<li><a href="#"><span class="status-mark position-left bg-primary"></span> Normal priority</a></li>
							<li><a href="#"><span class="status-mark position-left bg-success"></span> Low priority</a></li>
						</ul>
					</div>
				</td>
			</tr>
			<tr>
				<td><i class="icon-history position-left"></i> Revisions:</td>
				<td class="text-right">29</td>
			</tr>
			<tr>
				<td><i class="icon-file-plus position-left"></i> Added by:</td>
				<td class="text-right"><a href="#">Winnie</a></td>
			</tr>
			<tr>
				<td><i class="icon-file-check position-left"></i> Status:</td>
				<td class="text-right">Published</td>
			</tr>
		</tbody>
	</table>
	<div class="panel-footer panel-footer-condensed">
		<div class="heading-elements">
			<ul class="list-inline list-inline-condensed heading-text">
				<li><a href="#" class="text-default"><i class="icon-pencil7"></i></a></li>
				<li><a href="#" class="text-default"><i class="icon-bin"></i></a></li>
			</ul>
			<ul class="list-inline list-inline-condensed heading-text pull-right">
				<li><a href="#" class="text-default"><i class="icon-statistics"></i></a></li>
				<li class="dropdown">
					<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-gear"></i><span class="caret"></span></a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-alarm-add"></i> Check in</a></li>
						<li><a href="#"><i class="icon-attachment"></i> Attach screenshot</a></li>
						<li><a href="#"><i class="icon-user-plus"></i> Assign users</a></li>
						<li><a href="#"><i class="icon-warning2"></i> Report</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<!-- /task details -->
