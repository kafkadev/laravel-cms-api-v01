<!-- Main navbar -->
<div class="navbar navbar-inverse bg-indigo-400 navbar-transparent navbar-fixed-top" style="">
  <div class="navbar-boxed">
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img src="/assets/images/logo_light.png" alt=""></a>

      <ul class="nav navbar-nav pull-right visible-xs-block">
        <li><a data-toggle="collapse" data-target="#demo1"><i class="icon-tree5"></i></a></li>
      </ul>
    </div>

    <div class="navbar-collapse collapse" id="demo1">
      <ul class="nav navbar-nav">
        <li class="dropdown mega-menu mega-menu-wide active">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu7"></i>
            <span class="visible-xs-inline-block position-right">Navigation</span>
            <span class="caret"></span>
          </a>

          <div class="dropdown-menu dropdown-content">
            <div class="dropdown-content-body">

              <div class="row">
                <div class="col-md-3">
                  <span class="menu-heading underlined">pages</span>
                  <ul class="menu-list">
                    <li><a href="/home">home</a></li>
                    <li><a href="/contacts">contacts</a></li>
                    <li><a href="/classifieds">classifieds</a></li>
                    <li><a href="/articles">articles</a></li>
                    <li><a href="/single-article">single-article</a></li>
                    <li><a href="/personal">personal</a></li>
                    <li><a href="/business">business</a></li>
                    <li><a href="/messages">messages</a></li>
                    <li><a href="/manage">manage</a></li>
                  </ul>
                </div>
                <div class="col-md-3">
                  <span class="menu-heading underlined">pages</span>
                  <ul class="menu-list">
                    <li><a href="/business-activity">business-activity</a></li>
                    <li><a href="/personal-activity">personal-activity</a></li>
                    <li><a href="/profile-form">profile-form</a></li>
                  </ul>
                </div>
                <div class="col-md-3">
                  <span class="menu-heading underlined">Pages</span>
                  <ul class="menu-list">
                    <li><a href="/...">...</a></li>
                  </ul>
                </div>
                <div class="col-md-3">
                  <span class="menu-heading underlined">Empty</span>
                  <ul class="menu-list">
                    <li><a href="#">....</a></li>
                  </ul>
                </div>
              </div>

            </div>
          </div>
        </li>
        <li><a href="#"><i class="icon-envelop2 position-left"></i> Messages <span class="badge bg-indigo-800 badge-inline position-right">26</span></a></li>
        <li><a href="#"><i class="icon-cog3 position-left"></i> Settings</a></li>
      </ul>

      <div class="navbar-right">
        <form class="navbar-form navbar-left" action="#">
          <div class="form-group has-feedback">
            <input type="search" class="form-control input-xs" placeholder="Search field">
            <div class="form-control-feedback">
              <i class="icon-search4 text-muted text-size-base"></i>
            </div>
          </div>
        </form>

        <ul class="nav navbar-nav">
          <li class="dropdown language-switch">
            <a class="dropdown-toggle" data-toggle="dropdown">
              <img src="/assets/images/flags/gb.png"  class="position-left" alt=""> English
              <span class="caret"></span>
            </a>

            <ul class="dropdown-menu">
              <li class="active"><a class="english"><img src="/assets/images/flags/gb.png" alt=""> English</a></li>
              <li><a class="ukrainian"><img src="/assets/images/flags/ua.png" alt=""> Українська</a></li>
              <li><a class="deutsch"><img src="/assets/images/flags/de.png" alt=""> Deutsch</a></li>
              <li><a class="espana"><img src="/assets/images/flags/es.png" alt=""> España</a></li>
              <li><a class="russian"><img src="/assets/images/flags/ru.png" alt=""> Русский</a></li>
            </ul>
          </li>

          <li class="dropdown dropdown-user">
            <a class="dropdown-toggle" data-toggle="dropdown">
              <img src="/assets/images/placeholder.jpg" alt="">
              <span>Victoria</span>
              <i class="caret"></i>
            </a>

            <ul class="dropdown-menu dropdown-menu-right">
              <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
              <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
              <li><a href="#"><span class="badge badge-warning pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
              <li class="divider"></li>
              <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
              <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- /main navbar -->
