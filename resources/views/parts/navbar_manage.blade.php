<!-- Second navbar -->
<div class="navbar navbar-inverse navbar-transparent" id="navbar-second">
  <div class="navbar-boxed">
    <ul class="nav navbar-nav no-border visible-xs-block">
      <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
      <ul class="nav navbar-nav navbar-nav-material">
        <li><a href="/">Dashboard</a></li>

        <li class="dropdown mega-menu mega-menu-wide">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contents <span class="caret"></span></a>

          <div class="dropdown-menu dropdown-content">
            <div class="dropdown-content-body">
              <div class="row">
                <div class="col-md-3">
                  <span class="menu-heading underlined">Forms</span>
                  <ul class="menu-list"></ul>
                </div>									<div class="col-md-3">
                  <span class="menu-heading underlined">Forms</span>
                  <ul class="menu-list"></ul>
                </div>									<div class="col-md-3">
                  <span class="menu-heading underlined">Forms</span>
                  <ul class="menu-list"></ul>
                </div>									<div class="col-md-3">
                  <span class="menu-heading underlined">Forms</span>
                  <ul class="menu-list"></ul>
                </div>
              </div>
            </div>
          </div>
        </li>

        <li class="dropdown mega-menu mega-menu-wide">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Firends <span class="caret"></span></a>

          <div class="dropdown-menu dropdown-content">
            <div class="dropdown-content-body">

              <div class="row">
                <div class="col-md-3">
                  <span class="menu-heading underlined">Forms</span>
                  <ul class="menu-list"></ul>
                </div>									<div class="col-md-3">
                  <span class="menu-heading underlined">Forms</span>
                  <ul class="menu-list"></ul>
                </div>									<div class="col-md-3">
                  <span class="menu-heading underlined">Forms</span>
                  <ul class="menu-list"></ul>
                </div>									<div class="col-md-3">
                  <span class="menu-heading underlined">Forms</span>
                  <ul class="menu-list"></ul>
                </div>
              </div>
            </div>
          </div>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Messages <span class="caret"></span>
          </a>

          <ul class="dropdown-menu width-250">

          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Profile <span class="caret"></span>
          </a>

          <ul class="dropdown-menu width-200">

          </ul>
        </li>

      </ul>

      <ul class="nav navbar-nav navbar-nav-material navbar-right">
        <li>
          <a href="changelog.html">
            <span class="status-mark status-mark-inline border-success-300 position-left"></span>
            Changelog
          </a>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-cog3"></i>
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
            <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
            <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
            <li class="divider"></li>
            <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- /second navbar -->


<!-- Floating menu -->
<ul class="fab-menu fab-menu-top-right hide" data-fab-toggle="click">
  <li>
    <a class="fab-menu-btn btn bg-pink-300 btn-float btn-rounded btn-icon">
      <i class="fab-icon-open icon-plus3"></i>
      <i class="fab-icon-close icon-cross2"></i>
    </a>

    <ul class="fab-menu-inner">
      <li>
        <div data-fab-label="Compose email">
          <a href="#" class="btn btn-default btn-rounded btn-icon btn-float">
            <i class="icon-pencil"></i>
          </a>
        </div>
      </li>
      <li>
        <div data-fab-label="Conversations">
          <a href="#" class="btn btn-default btn-rounded btn-icon btn-float">
            <i class="icon-bubbles7"></i>
          </a>
          <span class="badge bg-primary-400">5</span>
        </div>
      </li>
      <li>
        <div data-fab-label="Chat with Jack">
          <a href="#" class="btn bg-pink-400 btn-rounded btn-icon btn-float">
            <img src="assets/images/placeholder.jpg" class="img-responsive" alt="">
          </a>
          <span class="status-mark border-pink-400"></span>
        </div>
      </li>
    </ul>
  </li>
</ul>
<!-- /floating menu -->
