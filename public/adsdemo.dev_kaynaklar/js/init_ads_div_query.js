/*
örnek kullanım
<div data-name="admanico" data-info="741852,363,96"></div>
<div data-name="admanico" data-info="963852,363,96"></div>
*/

const displayAds = {}
let requestData = {}
let currentData = {}
var currentFile = "";
var splData = "";
/*
Browser bağlantı özelliklerini alır. wifi, cellular (hücresel veri)
*/
displayAds.getConnection = () => {
  if (navigator.connection) {
    var type = navigator.connection.type;
    if(type) {
      if(type == "cellular") {
        return 'cellular';
      }else if(type == "wifi") {
        return 'wifi';
      }
    }
  } else {
    return 'lan';
  }
}

/*
Genel Browser Özelliklerini Alır (browser adı, dili, origin, v.b)
*/
displayAds.getBrowserData = () => {

  let browserData = {
    "platform" : navigator.platform,
    "language" : navigator.language,
    "browser" : navigator.vendor,
    "title" : 'bos',
    "width" : window.innerWidth,
    "height" : window.innerHeight,
    "pathname" : window.location.pathname,
    "origin" : window.location.origin,
    "host" : window.location.host,
    "hostname" : window.location.hostname,
    "protocol" : window.location.protocol,
    "network" : displayAds.getConnection()
  }
  Object.assign(requestData, browserData);
  return requestData;
}

/*
Sayfa içerisindeki dataları kullanarak reklam isteğinde bulunur
2 istek türü var ; ilki hash veya base64 olarak sunucuya get isteğinde bulunmak, diğeri obje post gönderimi,
yaptığım testlerde performans kaybı gözlemlemedim!
*/
displayAds.getFetch = (renderInfo) => {
  var renderData = displayAds.getBrowserData();
  Object.assign(renderData, renderInfo);
  var secret = btoa(JSON.stringify(renderData));
  //JSON.parse(atob(secret)); // {json: 1}
  //console.log(secret);
  fetch('//adsdemo.dev/api/reklam-goster/?sor=' + secret).then(function(response) {
    return response.json();
  }).then(function(data) {
    //var encodeData = JSON.parse(atob(data));
    displayAds.renderAd(renderInfo.divId, data);
    console.log(data);
  });

}

displayAds.postFetch = () => {
  fetch('http://adsdemo.dev/api/reklam-goster', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(displayAds.getBrowserData())
  }).then(function(response) {
    return response.json();
  }).then(function(data) {
    console.log(data);
  });

}

/* server tarafından gelen datayı render edip reklam gösterir. */
displayAds.renderAd = (divId, data) => {

if (data.image) {
 document.getElementById(divId).innerHTML = `<img src="${data.image}">`;
}

}

displayAds.getCurrentScript = () => {
  var currentFile = document.currentScript.getAttribute('data-info');
  var splData = currentFile.split(',');
return splData;
}
/*
Reklam elemanını mevcut script altına alır ve ilk çalışan metot.
*/
displayAds.firstRun = () => {
  var firstRun = document.currentScript;
  firstRun.insertAdjacentHTML('afterend', `<div id="${currentData.divId}"></div>`);
displayAds.getFetch();
}


displayAds.init = () => {
  var a = document.querySelectorAll('[data-name=admanico]');
  a.forEach(function(element) {
    var dataInfo = element.getAttribute('data-info');
    var splData = dataInfo.split(',');
    var divId = splData[0];
    element.id = divId;
    setTimeout(function () {
        displayAds.getFetch({"divId" : divId});
    }, 300);

      console.log(dataInfo);
  });


//  return true;
}
displayAds.init();
//displayAds.getCurrentScript();
//displayAds.firstRun();














/*

var datam = ``;
//document.getElementById("demo").innerHTML = datam;

//var myObject= {"firstName" : "Ahmet", "lastName" : "Karakule"};
setTimeout(function () {
//  w3.displayObject("id01", myObject);
w3.getHttpObject("http://adsdemo.dev/api/data-getir", myFunction);
}, 500);



function myFunction(myObject) {

myObject.kurallar = [{"sehir" : "izmir"}];
w3.displayObject("id01", myObject);
w3.displayObject("sayim", myObject.sayimlar);
w3.show('#id01');
}


function networkCheck() {
if (navigator.connection) {
var type = navigator.connection.type;
if(type) {
if(type == "cellular") {
document.getElementById("demo").innerHTML = "hucre";
}else if(type == "wifi") {
document.getElementById("demo").innerHTML = "wifi";
}else {
document.getElementById("demo").innerHTML = "anlaşılmadı";
}
}
} else {
document.getElementById("demo").innerHTML = "desteklenmiyor";
}
}

//networkCheck();


//console.log(datam);


*/
