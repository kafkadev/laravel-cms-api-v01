(function(){ if(typeof window.displayAds == 'undefined') {
  const displayAds = {}
  let requestData = {}
  let currentData = {}
  var currentFile = "";
  var splData = "";
  var defaultBanner = "http://www.liberyen.com/uploads/images/default.jpg";
  /*
  Browser bağlantı özelliklerini alır. wifi, cellular (hücresel veri)
  */
  displayAds.requestData = {}

  displayAds.typeTextImage = (responseData) => {
    return responseData;
  }
  displayAds.typeImage = (responseData) => {
    return responseData;
  }
  displayAds.typeJson = (responseData) => {
    return responseData;
  }
  displayAds.typePopup = (responseData) => {
    return responseData;
  }
  displayAds.typeVideo = (responseData) => {
    return responseData;
  }
  displayAds.typeInVideo = (responseData) => {
    return responseData;
  }
  displayAds.typeSlider = (responseData) => {
    return responseData;
  }
  displayAds.typeNews = (responseData) => {
    return responseData;
  }

  displayAds.getConnection = () => {
    if (navigator.connection) {
      var type = navigator.connection.type;
      if(type) {
        if(type == "cellular") {
          return 'cellular';
        }else if(type == "wifi") {
          return 'wifi';
        }
      }
    } else {
      return 'lan';
    }
  }
  /*
  Genel Browser Özelliklerini Alır (browser adı, dili, origin, v.b)
  */
  displayAds.getBrowserData = () => {
    let browserData = {
      "platform" : navigator.platform,
      "language" : navigator.language,
      "browser" : navigator.vendor,
      "title" : 'bos',
      "width" : window.innerWidth,
      "height" : window.innerHeight,
      "pathname" : window.location.pathname,
      "origin" : window.location.origin,
      "host" : window.location.host,
      "hostname" : window.location.hostname,
      "protocol" : window.location.protocol,
      "network" : displayAds.getConnection()
    }
    Object.assign(displayAds.requestData, browserData);
    return displayAds.requestData;
  }
  /*
  Sayfa içerisindeki dataları kullanarak reklam isteğinde bulunur
  2 istek türü var ; ilki hash veya base64 olarak sunucuya get isteğinde bulunmak, diğeri obje post gönderimi,
  yaptığım testlerde performans kaybı gözlemlemedim!
  */
  displayAds.getFetch = () => {
    var renderData = displayAds.getBrowserData();
    //  Object.assign(renderData);
    var secret = btoa(JSON.stringify(renderData));
    //JSON.parse(atob(secret)); // {json: 1}
    //console.log(secret);
    fetch('//www.liberyen.com/api/reklam-goster?sor=' + secret).then(function(response) {
      return response.json();
    }).then(function(data) {
      //var encodeData = JSON.parse(atob(data));
      displayAds.renderAd(data);
      //  console.log(data);
    });
  }
  displayAds.postFetch = () => {
    fetch('//www.liberyen.com/api/reklam-goster', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(displayAds.getBrowserData())
    }).then(function(response) {
      return response.json();
    }).then(function(data) {
      return data;
    });
  }
  /* server tarafından gelen datayı render edip reklam gösterir. */
  displayAds.renderAd = (data) => {
    if (data.image) {
      document.getElementById(displayAds.requestData.divId).innerHTML = `<img src="${data.image}">`;
    } else {
      document.getElementById(displayAds.requestData.divId).innerHTML = `<img src="${defaultBanner}">`;
    }

  }
  displayAds.getCurrentScript = (element) => {
    //  var thisFile = document.currentScript;
    var currentFile = element.getAttribute('data-info');
    var splData = currentFile.split(',');
    displayAds.requestData['divId'] = splData[0];
    displayAds.requestData['user_id'] = splData[1];
    displayAds.requestData['kampanya_id'] = splData[2];
    displayAds.requestData['reklam_id'] = splData[3];
    //  console.log(displayAds.requestData);
    element.insertAdjacentHTML('afterend', `<div id="${splData[0]}"></div>`);
    displayAds.getFetch();
    //return splData;
  }
  displayAds.getCurrentScript(document.currentScript);
}})();
/*
Reklam elemanını mevcut script altına alır ve ilk çalışan metot.
displayAds.firstRun = () => {
}
displayAds.init = () => {
var a = document.querySelectorAll('[data-name=admanico]');
a.forEach(function(element) {
var dataInfo = element.getAttribute('data-info');
var splData = dataInfo.split(',');
var divId = splData[0];
element.id = divId;
setTimeout(function () {
displayAds.getFetch({"divId" : divId});
}, 300);
});
}
var datam = ``;
//document.getElementById("demo").innerHTML = datam;
//var myObject= {"firstName" : "Ahmet", "lastName" : "Karakule"};
setTimeout(function () {
//  w3.displayObject("id01", myObject);
w3.getHttpObject("http://adsdemo.dev/api/data-getir", myFunction);
}, 500);
function myFunction(myObject) {
myObject.kurallar = [{"sehir" : "izmir"}];
w3.displayObject("id01", myObject);
w3.displayObject("sayim", myObject.sayimlar);
w3.show('#id01');
}
function networkCheck() {
if (navigator.connection) {
var type = navigator.connection.type;
if(type) {
if(type == "cellular") {
document.getElementById("demo").innerHTML = "hucre";
}else if(type == "wifi") {
document.getElementById("demo").innerHTML = "wifi";
}else {
document.getElementById("demo").innerHTML = "anlaşılmadı";
}
}
} else {
document.getElementById("demo").innerHTML = "desteklenmiyor";
}
}
//networkCheck();
//console.log(datam);
*/
