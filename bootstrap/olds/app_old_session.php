<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}



$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);






$app->withFacades();
/*$app->withFacades(true, [
    Illuminate\Support\Facades\Session::class => "Session",
]);*/

$app->configure('filesystems');
$app->configure('cache');
$app->configure('database');
$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);



$app->singleton('filesystem', function ($app) {
    return $app->loadComponent('filesystems', Illuminate\Filesystem\FilesystemServiceProvider::class, 'filesystem');
});


/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
]);


$app->routeMiddleware([
     'dem' => App\Http\Middleware\AuthToken::class,
     'auth' => App\Http\Middleware\Authenticate::class,
     //'dem' => App\Http\Middleware\Authenticate::class,
]);





/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// $app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
$app->register(App\Providers\CustomServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);
$app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/


$app->bind(Illuminate\Session\SessionManager::class, function ($app) {
    return new Illuminate\Session\SessionManager($app);
});

$app->middleware([
    Illuminate\Session\Middleware\StartSession::class,
]);

$app->register(Illuminate\Session\SessionServiceProvider::class);
$app->bind(Illuminate\Session\SessionManager::class, function ($app) {    
    return $app->make('session');
});
$app->configure('session');




$app->group(['prefix' => 'api','namespace' => 'App\Http\Controllers\Api'], function ($app) {
    require __DIR__.'/../routes/api.php';
});


$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {

    require __DIR__.'/../routes/web.php';
});

return $app;
