<?php
use Illuminate\Http\Request;

$app->get('/', function () {
  //return view('pages/manage');
  return response()->json(array("message" => "development area"), 200);
});

/*
$app->get('/kampanyalar', function () {
  return view('pages/kampanyalar');
});
$app->get('/login', function () {
  return view('pages.login');
});




$app->group(['prefix' => 'reklamlar' ], function($app)  {
  $app->get('/ekle', function (Request $request) use ($app) {
    return view('pages/kampanya_edit');
  });
  $app->get('/{reklamId}', function (Request $request) use ($app) {
    return view('pages/reklam_goster');
  });
  $app->get('/', function (Request $request) use ($app) {
    return view('pages/reklamlar');
  });
});



$app->group(['prefix' => 'kampanyalar' ], function($app)  {
  $app->get('/ekle', function (Request $request) use ($app) {
    return view('pages.kampanya.kampanya_ekle', array('kampanyaId' => '1'));
  });

  $app->group(['prefix' => '/{kampanyaId}'], function ($app) {
  // $app->get('/[{page_name}]', function ()    {
  //   return view('pages.index');
  // });
  $app->get('/', function (Request $request, $kampanyaId) use ($app) {
    return view('pages.kampanya.kampanya_first', array('kampanyaId' => $kampanyaId));
  });
  $app->get('/raporlar', function (Request $request) use ($app) {
    return view('pages.kampanya.rapor');
  });
  $app->get('/lokasyon', function (Request $request) use ($app) {
    return view('pages.kampanya.lokasyon');
  });
  $app->get('/platform', function (Request $request) use ($app) {
    return view('pages.kampanya.platform');
  });
  $app->get('/zamanlama', function (Request $request) use ($app) {
    return view('pages.kampanya.zamanlama');
  });
  $app->get('/filtreler', function (Request $request) use ($app) {
    return view('pages.kampanya.filtreler');
  });
  $app->get('/demo-form', function (Request $request, $kampanyaId) use ($app) {
    return view('pages.kampanya.demo-form', array('kampanyaId' => $kampanyaId));
  });
  $app->get('/reklamlar', function (Request $request, $kampanyaId) use ($app) {
    $tum_reklamlar = app('db')->table('reklamlar')->where('kampanya_id', $kampanyaId)->get();
    return view('pages.kampanya.reklamlar', array('kampanyaId' => $kampanyaId,'tum_reklamlar' => $tum_reklamlar));
  });

  $app->get('/reklamlar/ekle', function (Request $request, $kampanyaId) use ($app) {
    return view('pages/reklam_goster', array('kampanyaId' => $kampanyaId));
  });
  $app->get('/reklamlar/{reklamId}', function (Request $request, $kampanyaId, $reklamId) use ($app) {
    $reklam = app('db')->table('reklamlar')->where('id', $reklamId)->where('kampanya_id', $kampanyaId)->first();
    return view('pages.kampanya.reklam', array('reklam' => $reklam, 'kampanyaId' => $kampanyaId));
  });
  $app->get('/yonetim', function (Request $request) use ($app) {
    return view('pages.kampanya.kampanya_first');
  });
  });

  $app->get('/', function (Request $request) use ($app) {
  $tum_kampanyalar = app('db')->table('kampanyalar')->where('user_id', 1)->get();
    return view('pages.kampanya.kampanyalar', array('tum_kampanyalar' => $tum_kampanyalar));
  });

});
*/
