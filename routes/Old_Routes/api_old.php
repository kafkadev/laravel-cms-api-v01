<?php
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserMeta;
$app->post('postlogin', 'AuthenticateController@postLogin');
$app->group(['middleware' => 'auth:api' ], function($app)  {

    $app->get('addcampaign', function (Request $request) use ($app) {
        return "add campaign route";
    });
});

$app->post('/generator-update', function (Request $request) use ($app) {
  $formlar = $request->input('formlar');
  //1 way
  $datam = [];
  foreach ($formlar as $key => $item) {
    $datam[] = UserMeta::updateOrCreate([
      'user_id'   => 1,
      'meta_name' => $key
  ],[
    'meta_name' => $key,
    'meta_value' => $item
  ]);
  }

return response()->json($datam, 200);
});

$app->get('/friends', function (Request $request) use ($app) {
//http://lumensession.dev/api/friends?initiator=1&user=2&confirmed=0

$datam = DB::table('friends')->updateOrInsert([
    'initiator_user_id'   => $request->input('initiator'),
    'friend_user_id' => $request->input('user')
],[
  'is_confirmed' => $request->input('confirmed')
]);

return response()->json($datam, 200);
});

$app->get('/generator-user', function () use ($app) {
$data = [];
  $user_info = DB::table('user_metas')->where('user_id', 1)->where('meta_name', 'user_info')->first();
  $user_all = DB::table('user_metas')->whereNotIn('meta_name', ['user_info'])->where('user_id', 1)->pluck('meta_value', 'meta_name' );;
  $data['user_info'] = unserialize($user_info->meta_value);
  $data['user_all'] = $user_all;

return response()->json($data, 200);
});



$app->get('/makale', function () use ($app) {
$rand = rand(1,8);
$data = Cache::get('all_articles'.$rand);
if (!$data) {
$getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/articles?page=".$rand);
$data = json_decode($getArticle);
Cache::put('all_articles' . $rand, $data, 3600);
}
return response()->json($data, 200);
});

$app->get('/makale-tek/{id}', function ($ids) use ($app) {
$data = Cache::get('articles' . $ids);
if (!$data) {
$getArticle = file_get_contents("http://restwp.dev/wp-json/wp/v2/articles/$ids");
$data = json_decode($getArticle);
Cache::put('articles' . $ids, $data, 3600);
}
return response()->json($data, 200);
});

$app->get('/wordpress', function () use ($app) {
$users = DB::connection('wordpress')->table('posts')->limit(5)->get();
return response()->json($users, 200);
});

$app->get('/getir', ['middleware' => 'auth', function (Request $request) {
  $user = Auth::user();
   $user = $request->user();
return response()->json($user, 200);
}]);




$app->get('/form', function () use ($app) {
$data = [];
$getArticle = DB::table('wp_posts')->where('post_type', '=', 'acf-field-group')->get();
foreach ($getArticle as $key => $value) {
$content = (object)[];
$content->title = $value->post_title;
$content->id = $value->ID;
$content->status = $value->post_status;
$content->content = unserialize($value->post_content);
$content->fields = [];
$getFields = DB::table('wp_posts')->where('post_parent', '=', $value->ID)->get();
foreach ($getFields as $fieldKey => $fieldValue) {
$content->fields[$fieldKey] = (object)[];
$content->fields[$fieldKey]->id = $fieldValue->ID;
$content->fields[$fieldKey]->name = $fieldValue->post_title;
$content->fields[$fieldKey]->status = $fieldValue->post_status;
$content->fields[$fieldKey]->field = @getirBak($fieldValue->post_content);
}
foreach (unserialize($value->post_content) as $locKey) {
$loca = @(object)$locKey[0][0];
if (isset($loca, $loca->param) && $loca->param == 'post_taxonomy') {
$term = DB::table('wp_terms')->where('slug', '=', explode(':', $loca->value)[1])->first();
$content->terms = deepCategory($term);
}
	}

$data[] = $content;
}

return response()->json($data, 200);
});


function getirBak($datam){
	return (array)unserialize($datam);
}
function deepCategory($term){
	$a = [];
	$d = DB::table('wp_term_taxonomy')->where('term_id', '=', $term->term_id)->value('parent');
	$a[] = $term->term_id;
	$a[] = $d;

while($d)
{
	$d = DB::table('wp_term_taxonomy')->where('term_id', '=', $d)->value('parent');
	if ($d) {
	$a[] = $d;
	}

}
return $a;
}
